package src.task2;

public class Time implements Comparable<Time> {
    private int hours;
    private int minutes;
    private int seconds;

    public Time(int hours, int minutes, int seconds) {
        setHours(hours);
        setMinutes(minutes);
        setSeconds(seconds);
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        if (!(0 <= hours && hours < 24)) {
            this.hours = 0;
            return;
        }

        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        if (!(0 <= minutes && minutes < 60)) {
            this.minutes = 0;
            return;
        }

        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        if (!(0 <= seconds && seconds < 60)) {
            this.seconds = 0;
            return;
        }

        this.seconds = seconds;
    }

    public void add(Time time) {
        seconds += time.seconds;
        minutes += seconds / 60;
        seconds %= 60;

        minutes += time.minutes;
        hours += minutes / 60;
        minutes %= 60;

        hours += time.hours;
        hours %= 24;
    }

    public void sub(Time time) {
        seconds -= time.seconds;
        if (seconds < 0) {
            minutes -= 1;
            seconds += 60;
        }

        minutes -= seconds / 60;
        seconds %= 60;

        minutes -= time.minutes;
        if (minutes < 0) {
            hours -= 1;
            minutes += 60;
        }

        hours -= minutes / 60;
        minutes %= 60;

        hours -= time.hours;
        if (hours < 0) {
            hours += 24;
        }
        hours %= 24;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        var time = (Time) object;
        return hours == time.getHours()
            && minutes == time.getMinutes()
            && seconds == time.getSeconds();
    }

    @Override
    public int compareTo(Time time) {
        if (this == time) {
            return 0;
        }

        if (time == null) {
            return 1;
        }

        if (hours == time.getHours()) {
            if (minutes == time.getMinutes()) {
                if (seconds == time.getSeconds()) {
                    return 0;
                }

                return Integer.compare(seconds, time.getSeconds());
            }

            return Integer.compare(minutes, time.getMinutes());
        }

        return Integer.compare(hours, time.getHours());
    }

    @Override
    public String toString() {
        return String.format("%02d:%02d:%02d", this.hours, this.minutes, this.seconds);
    }
}