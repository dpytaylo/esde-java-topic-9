package src.task2;

import java.util.Comparator;

public class TrainComparatorByTrainNumber implements Comparator<Train> {
    @Override
    public int compare(Train train, Train train2) {
        if (train == train2) {
            return 0;
        }

        if(train == null) {
            return -1;
        }
        else if (train2 == null) {
            return 1;
        }

        return Integer.compare(train.getTrainNumber(), train2.getTrainNumber());
    }
}