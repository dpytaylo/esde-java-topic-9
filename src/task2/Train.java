package src.task2;

import java.util.Scanner;

public class Train {
    private String destinationName;
    private int trainNumber;
    private Time departureTime;

    public Train(String destinationName, int trainNumber, Time departureTime) {
        this.setDestinationName(destinationName);
        this.setTrainNumber(trainNumber);
        this.setDepartureTime(departureTime);
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(int trainNumber) {
        this.trainNumber = trainNumber;
    }

    public Time getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Time departureTime) {
        this.departureTime = departureTime;
    }

    @Override
    public String toString() {
        return String.format(
            "(%s, %d, %s)",
                getDestinationName(),
                getTrainNumber(),
                getDepartureTime()
        );
    }
}