package src.task2;

import java.util.Comparator;

public class TrainComparatorByDestination implements Comparator<Train>  {
    @Override
    public int compare(Train train, Train train2) {
        if (train == train2) {
            return 0;
        }

        if(train == null) {
            return -1;
        }
        else if (train2 == null) {
            return 1;
        }

        int result = train.getDestinationName().compareTo(train2.getDestinationName());

        if (result == 0) {
            return train.getDepartureTime().compareTo(train2.getDepartureTime());
        }

        return result;
    }
}