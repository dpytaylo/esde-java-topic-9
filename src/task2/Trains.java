package src.task2;

import java.util.Scanner;

public class Trains {
    public static void getTrainByNumberFromConsole(Train[] trains) {
        var scanner = new Scanner(System.in);

        System.out.println("Please, enter the train number: ");
        int number = scanner.nextInt();

        boolean wasTrain = false;
        for (Train train : trains) {
            if (train.getTrainNumber() == number) {
                System.out.format("Train info:\n\tdestinationName = %s\n\ttrainNumber = %s\n\tdepartureTime = %s\n",
                        train.getDestinationName(),
                        train.getTrainNumber(),
                        train.getDepartureTime()
                );

                wasTrain = true;
                break;
            }
        }

        if (!wasTrain) {
            System.out.println("No train info");
        }
    }
}
