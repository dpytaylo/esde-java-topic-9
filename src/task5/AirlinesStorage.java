package src.task5;

import src.task2.Time;

import java.util.ArrayList;

public class AirlinesStorage {
    private final ArrayList<Airline> inner;

    public AirlinesStorage() {
        inner = new ArrayList<>();
    }

    public void add(Airline airline) {
        inner.add(airline);
    }

    @Override
    public String toString() {
        return inner.toString();
    }

    public ArrayList<Airline> getFlightsByDestination(String destination) {
        var result = new ArrayList<Airline>();

        for (var airline : inner) {
            if (airline == null) {
                continue;
            }

            if (airline.getDestination().equals(destination)) {
                result.add(airline);
            }
        }

        return result;
    }

    public ArrayList<Airline> getFlightsByDayOfWeek(int dayOfWeek) {
        var result = new ArrayList<Airline>();

        for (var airline : inner) {
            if (airline == null) {
                continue;
            }

            if (airline.getDaysOfWeek() == dayOfWeek) {
                result.add(airline);
            }
        }

        return result;
    }

    public ArrayList<Airline> getFlightsByDayOfWeekWithDepartureTime(int dayOfWeek, Time departureTime) {
        var result = new ArrayList<Airline>();

        for (var airline : inner) {
            if (airline == null) {
                continue;
            }

            if (airline.getDaysOfWeek() == dayOfWeek && airline.getDepartureTime().compareTo(departureTime) > 0) {
                result.add(airline);
            }
        }

        return result;
    }
}
