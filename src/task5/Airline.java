package src.task5;

import src.task2.Time;

public class Airline {
    private String destination;
    private int flightNumber;
    private String aircraftType;
    private Time departureTime;
    private int daysOfWeek;

    public Airline(
        String destination,
        int flightNumber,
        String aircraftType,
        Time departureTime,
        int daysOfWeek
    ) {
        this.destination = destination;
        this.flightNumber = flightNumber;
        this.aircraftType = aircraftType;
        this.departureTime = departureTime;
        this.daysOfWeek = daysOfWeek;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAircraftType() {
        return aircraftType;
    }

    public void setAircraftType(String aircraftType) {
        this.aircraftType = aircraftType;
    }

    public Time getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Time departureTime) {
        this.departureTime = departureTime;
    }

    public int getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(int daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    @Override
    public String toString() {
        return String.format(
            "{destination: %s; flightNumber: %d; aircraftType: %s; "
            + "departureType: %s; daysOfWeek: %d}",
            destination, flightNumber, aircraftType,
            departureTime, daysOfWeek
        );
    }
}