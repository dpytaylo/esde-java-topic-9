package src;

import src.task1.Student;
import src.task2.*;
import src.task3.Customer;
import src.task3.CustomersStorage;
import src.task4.Book;
import src.task4.BooksStorage;
import src.task5.Airline;
import src.task5.AirlinesStorage;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        //task1();
        //task2();
        //task3();
        task4();
        //task5();
    }

    private static void task1() {
        var students = new Student[10];

        students[0] = new Student("Pupkin", "V.B", 1, new int[] {3, 4, 5, 2, 1});
        students[1] = new Student("Talkchau", "M.B", 1, new int[] {1, 3, 9, 2, 1});
        students[2] = new Student("Demidovich", "D.D", 1, new int[] {3, 5, 5, 5, 1});
        students[3] = new Student("Amogus", "A.B", 1, new int[] {2, 1, 5, 2, 1});
        students[4] = new Student("Papug", "G.G", 1, new int[] {9, 10, 9, 9, 10});

        students[5] = new Student("Maksimovich", "M.M", 1, new int[] {2, 4, 5, 2, 1});
        students[6] = new Student("Radchenko", "R.A", 1, new int[] {9, 10, 10, 9, 9});
        students[7] = new Student("Schemovich", "P.D", 1, new int[] {3, 4, 5, 2, 2});
        students[8] = new Student("Persikov", "H.B", 1, new int[] {10, 10, 10, 9, 10});
        students[9] = new Student("Ivanvich", "I.A", 1, new int[] {1, 4, 5, 2, 4});

        for (int i = 0; i < 10; i++) {
            if (students[i].hasGoodMarks()) {
                System.out.println(students[i]);
            }
        }
    }

    private static void task2() {
        Train[] trains = new Train[5];
        trains[0] = new Train("New York City", 2054, new Time(7, 30, 2));
        trains[1] = new Train("San Francisco", 9813, new Time(10, 15, 1));
        trains[2] = new Train("Houston", 4523, new Time(16, 0, 5));
        trains[3] = new Train("Houston", 5005, new Time(14, 30, 2));
        trains[4] = new Train("Chicago", 5087, new Time(14, 55, 43));
        //trains[4] = null;

        var trainsCloned = trains.clone();
        Arrays.sort(trainsCloned, new TrainComparatorByTrainNumber());
        System.out.println(Arrays.toString(trainsCloned));

        //Trains.getTrainByNumberFromConsole(trains);

        Arrays.sort(trains, new TrainComparatorByDestination());
        System.out.println(Arrays.toString(trains));
    }

    private static void task3() {
        var customers = new CustomersStorage();
        customers.add(new Customer(1, "Smith", "John", "J", "123 Main St, Anytown USA", 1234567812, 123456));
        customers.add(new Customer(2, "Jones", "Jane", "D", "456 Park Ave, Anycity USA", 23456789, 98765));
        customers.add(new Customer(3, "Williams", "Bob", "P", "789 Elm St, Anyvillage USA", 345678934, 111111));
        customers.add(new Customer(4, "Jones", "Jane", "A", "321 Oak Rd, Anystate USA", 456789567, 222222));
        customers.add(new Customer(5, "Brown", "Emily", "G", "159 Pine St, Anycountry USA", 567896789, 333333));

        System.out.println(customers);
        System.out.println();

        var array = customers.getCustomersInAlphabetOrder();
        for (Customer element : array) {
            System.out.println(element);
        }

        System.out.println();

        var array2 = customers.getCustomersWithCreditCardNumberInInterval(345678934, 500000000);
        for (Customer element : array2) {
            System.out.println(element);
        }
    }

    private static void task4() {
        var books = new BooksStorage();
        books.add(new Book(1, "Harry Potter and the Philosopher's Stone", new String[]{"J.K. Rowling"}, "Bloomsbury Publishing", 1997, 223, 10, "Hardcover"));
        books.add(new Book(2, "The Great Gatsby", new String[]{"F. Scott Fitzgerald"}, "J.B. Lippincott & Co.", 1925, 180, 15, "Paperback"));
        books.add(new Book(3, "To Kill a Mockingbird", new String[]{"F. Scott Fitzgerald"}, "Charles Scribner's Sons", 1960, 324, 12, "Hardcover"));
        books.add(new Book(4, "The Catcher in the Rye", new String[]{"J.D. Salinger"}, "Little, Brown and Company", 1951, 277, 9, "Paperback"));
        books.add(new Book(5, "The Lord of the Rings", new String[]{"J.R.R. Tolkien"}, "J.B. Lippincott & Co.", 1954, 1178, 20, "Hardcover"));

        System.out.println(books);
        System.out.println();

        System.out.println(books.getBooksByAuthor("F. Scott Fitzgerald"));
        System.out.println(books.getBooksByPublisher("J.B. Lippincott & Co."));
        System.out.println(books.getBooksAfterGivenYear(1951));
    }

    private static void task5() {
        var airlines = new AirlinesStorage();
        airlines.add(new Airline("Tokyo", 1234, "Boeing 747", new Time(8, 30, 0), 2));
        airlines.add(new Airline("Los Angeles", 5678, "Airbus A320", new Time(12, 15, 0), 5));
        airlines.add(new Airline("Paris", 9012, "Boeing 777", new Time(16, 45, 0), 3));
        airlines.add(new Airline("Tokyo", 3456, "Airbus A330", new Time(20, 30, 0), 3));
        airlines.add(new Airline("Sydney", 7890, "Boeing 787", new Time(6, 0, 0), 1));

        System.out.println(airlines);
        System.out.println();

        System.out.println(airlines.getFlightsByDestination("Tokyo"));
        System.out.println(airlines.getFlightsByDayOfWeek(3));
        System.out.println(airlines.getFlightsByDayOfWeekWithDepartureTime(3, new Time(17, 23, 50)));
    }
}
