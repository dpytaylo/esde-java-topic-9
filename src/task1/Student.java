package src.task1;

import java.util.Arrays;

public class Student {
    private final String surname;
    private final String initials;
    private final int groupNumber;
    private final int[] academicPerformance;

    public Student(String surname, String initials, int groupNumber, int[] academicPerformance) {
        this.surname = surname;
        this.initials = initials;
        this.groupNumber = groupNumber;
        this.academicPerformance = academicPerformance;
    }

    public boolean hasGoodMarks() {
        for (int value : academicPerformance) {
            if (value < 9) {
                return false;
            }
        }

        return true;
    }

    @Override
    public String toString() {
        return String.format(
            "{surname: %s; initials: %s; groupNumber: %s; academicPerformance: %s}",
            surname, initials, groupNumber,
            Arrays.toString(academicPerformance)
        );
    }
}
