package src.task4;

import java.util.ArrayList;

public class BooksStorage {
    private final ArrayList<Book> inner;

    public BooksStorage() {
        inner = new ArrayList<>();
    }

    public void add(Book book) {
        inner.add(book);
    }

    public ArrayList<Book> getBooksByAuthor(String author) {
        var result = new ArrayList<Book>();

        for (Book book : inner) {
            if (book == null) {
                continue;
            }

            for (var bookAuthor : book.getAuthors()) {
                if (bookAuthor == null) {
                    continue;
                }

                if (bookAuthor.equals(author)) {
                    result.add(book);
                    break;
                }
            }
        }

        return result;
    }

    public ArrayList<Book> getBooksByPublisher(String publisher) {
        var result = new ArrayList<Book>();

        for (Book book : inner) {
            if (book == null) {
                continue;
            }

            if (publisher.equals(book.getPublisher())) {
                result.add(book);
            }
        }

        return result;
    }

    public ArrayList<Book> getBooksAfterGivenYear(int year) {
        var result = new ArrayList<Book>();

        for (Book book : inner) {
            if (book == null) {
                continue;
            }

            if (book.getYearOfPublication() > year) {
                result.add(book);
            }
        }

        return result;
    }

    @Override
    public String toString() {
        return inner.toString();
    }
}
