package src.task4;

import java.util.Arrays;

public class Book {
    private int id;
    private String title;
    private String[] authors;
    private String publisher;
    private int yearOfPublication;
    private int numberOfPages;
    private int price;
    private String typeOfBinding;

    public Book(
        int id,
        String title,
        String[] authors,
        String publisher,
        int yearOfPublication,
        int numberOfPages,
        int price,
        String typeOfBinding
    ) {
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.publisher = publisher;
        this.yearOfPublication = yearOfPublication;
        this.numberOfPages = numberOfPages;
        this.price = price;
        this.typeOfBinding = typeOfBinding;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getAuthors() {
        return authors;
    }

    public void setAuthors(String[] authors) {
        this.authors = authors;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getYearOfPublication() {
        return yearOfPublication;
    }

    public void setYearOfPublication(int yearOfPublication) {
        this.yearOfPublication = yearOfPublication;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getTypeOfBinding() {
        return typeOfBinding;
    }

    public void setTypeOfBinding(String typeOfBinding) {
        this.typeOfBinding = typeOfBinding;
    }

    @Override
    public String toString() {
        return String.format(
            "{id: %d; title: %s; authors: %s; publisher: %s; "
            + "yearOfPublication: %d; numberOfPages: %d; "
            + "price: %d; typeOfBinding: %s}",
            id, title, Arrays.toString(authors), publisher, yearOfPublication,
            numberOfPages, price, typeOfBinding
        );
    }
}
