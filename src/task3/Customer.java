package src.task3;

public class Customer implements Comparable<Customer> {
    private int id;
    private String surname;
    private String firstName;
    private String patronymic;
    private String address;
    private int creditCardNumber;
    private int bankAccountNumber;

    public Customer(
        int id,
        String surname,
        String firstName,
        String patronymic,
        String address,
        int creditCardNumber,
        int bankAccountNumber
    ) {
        this.id = id;
        this.surname = surname;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.address = address;
        this.creditCardNumber = creditCardNumber;
        this.bankAccountNumber = bankAccountNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(int creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public int getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(int bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    @Override
    public String toString() {
        return String.format("" +
            "{id: %d; surname: %s; firstName: %s; patronymic: %s; " +
            "address: %s; creditCardNumber: %d; bankAccountNumber: %d}",
            id,
            surname,
            firstName,
            patronymic,
            address,
            creditCardNumber,
            bankAccountNumber
        );
    }

    @Override
    public int compareTo(Customer customer) {
        var result = surname.compareTo(customer.surname);

        if (result == 0) {
            var result2 = firstName.compareTo(customer.firstName);

            if (result2 == 0) {
                return patronymic.compareTo(customer.patronymic);
            }

            return result2;
        }

        return result;
    }
}